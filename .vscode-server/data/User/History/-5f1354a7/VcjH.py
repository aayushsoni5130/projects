# Exercise 42: Is-A, Has-A, Objects, and Classes

# Define a base class 'Animal'
class Animal(object):
    pass

# Define a subclass 'Dog' which inherits from 'Animal'
class Dog(Animal):
    def __init__(self, name):
        # Initialize the 'name' attribute for the Dog instance
        self.name = name

# Define a subclass 'Cat' which inherits from 'Animal'
class Cat(Animal):
    def __init__(self, name):
        # Initialize the 'name' attribute for the Cat instance
        self.name = name

# Define a base class 'Person'
class Person(object):
    def __init__(self, name):
        # Initialize the 'name' attribute for the Person instance
        self.name = name
        # Initialize the 'pet' attribute for the Person instance
        self.pet = None

# Define a subclass 'Employee' which inherits from 'Person'
class Employee(Person):
    def __init__(self, name, salary):
        # Call the constructor of the parent class 'Person'
        super(Employee, self).__init__(name)
        # Initialize the 'salary' attribute for the Employee instance
        self.salary = salary

# Define a base class 'Fish'
class Fish(object):
    pass

# Define a subclass 'Salmon' which inherits from 'Fish'
class Salmon(Fish):
    pass

# Define a subclass 'Halibut' which inherits from 'Fish'
class Halibut(Fish):
    pass

# Create an instance 'rover' of class 'Dog' with the name "Rover"
rover = Dog("Rover")

# Create an instance 'satan' of class 'Cat' with the name "Satan"
satan = Cat("Satan")

# Create an instance 'mary' of class 'Person' with the name "Mary"
mary = Person("Mary")

# Associate the 'pet' attribute of 'mary' with the 'satan' instance
mary.pet = satan

# Create an instance 'frank' of class 'Employee' with the name "Frank" and salary 120000
frank = Employee("Frank", 120000)

# Associate the 'pet' attribute of 'frank' with the 'rover' instance
frank.pet = rover

# Create an instance 'flipper' of class 'Fish'
flipper = Fish()

# Create an instance 'crouse' of class 'Salmon'
crouse = Salmon()

# Create an instance 'harry' of class 'Halibut'
harry = Halibut()
