### for different Operators in python 

```
import operator

# Add two numbers
result = operator.add(1, 2)

# Subtract two numbers
result = operator.sub(1, 2)

# Multiply two numbers
result = operator.mul(1, 2)

# Divide two numbers
result = operator.truediv(1, 2)

# Modulo two numbers
result = operator.mod(1, 2)

# Power two numbers
result = operator.pow(1, 2)

# Bitwise AND two numbers
result = operator.and_(1, 2)

# Bitwise OR two numbers
result = operator.or_(1, 2)

# Bitwise XOR two numbers
result = operator.xor(1, 2)

# Bitwise NOT a number
result = operator.not_(1)

# Bitwise left shift a number
result = operator.lshift(1, 2)

# Bitwise right shift a number
result = operator.rshift(1, 2)

```