# Define a string with items separated by spaces.
ten_things = "Apples Oranges Crows Telephone Light Sugar"

# Print a message indicating there are not 10 things in the list.
print("Wait there's not 10 things in that list, let's fix that.")

# Split the string into individual words using space as a separator,
# and store them in a list called 'stuff'.
stuff = ten_things.split(' ')

# Create another list called 'more_stuff' containing additional items.
more_stuff = ["Day", "Night", "Song", "Frisbee", "Corn", "Banana", "Girl", "Boy"]

# Use a while loop to add items from 'more_stuff' to 'stuff' until 'stuff' contains 10 items.
while len(stuff) != 10:
    # Remove the last item from 'more_stuff' and store it in 'next_one'.
    next_one = more_stuff.pop()
    # Print the item being added.
    print("Adding: ", next_one)
    # Append the 'next_one' item to the 'stuff' list.
    stuff.append(next_one)
    # Print the current number of items in 'stuff'.
    print(f"There's {len(stuff)} items now.")

# Print the final list 'stuff' with 10 items.
print("There we go: ", stuff)

# Print some specific elements from the 'stuff' list.
print("Let's do some things with stuff.")
print(stuff[1])  # Print the item at index 1 (second item).
print(stuff[-1])  # Print the last item using negative indexing (the last item in the list).
print(stuff.pop())  # Remove and print the last item in the list.
print(' '.join(stuff))  # Join all items in the list with spaces and print them.
print('#'.join(stuff[3:5]))  # Join items from index 3 (inclusive) to index 5 (exclusive) with '#' and print them.
