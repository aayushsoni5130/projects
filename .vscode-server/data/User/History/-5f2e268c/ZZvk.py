# Exercise 25: Even More Practice

def break_words(stuff):
    """This function will break up words for us"""
    words = stuff.split(' ')  # Split the input string (stuff) into individual words using space as the separator
    return words  # Return the list of words

def sort_words(words):
    """Sort the words"""
    return sorted(words)  # Return a new list with words sorted in alphabetical order

def print_first_word(words):
    """Print the first word after popping it off"""
    word = words.pop(0)  # Remove and store the first word (index 0) from the list (words)
    print(word)  # Print the first word

def print_last_word(words):
    """Print the last word after popping it off"""
    word = words.pop(-1)  # Remove and store the last word (last index) from the list (words)
    print(word)  # Print the last word

def sort_sentence(sentence):
    """Takes the full sentence and returns the sorted words."""
    words = break_words(sentence)  # Split the sentence into words using break_words function
    return sorted(words)  # Return a new list with words sorted in alphabetical order

def print_first_and_last(sentence):
    """Print the first and last words of a sentence"""
    words = break_words(sentence)  # Split the sentence into words using break_words function
    print_first_word(words)  # Print the first word of the sentence
    print_last_word(words)  # Print the last word of the sentence

# Test the functions
sentence = "Hello world, how are you doing today?"
words = break_words(sentence)
print(words)  # Output: ['Hello', 'world,', 'how', 'are', 'you', 'doing', 'today?']

sorted_words = sort_words(words)
print(sorted_words)  # Output: ['Hello', 'are', 'doing', 'how', 'today?', 'world,', 'you']

print_first_and_last(sentence)  # Output: Hello today?
