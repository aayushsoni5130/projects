# Exercise 25: Even More Practice

def break_words(stuff):
	"""This function will break  up words for us"""
	words = stuff.split(' ')
	return words

def sort_words(words):
	"""Sort the words"""
	return sorted(words)

def print_first_word(words):
	"""print the first word after popping it of """
	word = words.pop(0)
	print(word)

def print_last_word(words):
	"""print the last word after poping it of"""
	word = words.pop(-1)
	print(word)

def print_last_word(words):
	"""Print the last word after popping it off"""
	word = words.pop(0)
	print(word)

def sort_sentence(sentence):
	"""Takes the full sentence and retuen the short words. """
	words = break_words(sentence)
	return (sort_words(words))

def print_first_and_last(sentence):
	"""Print the first and last words of a sentece"""
	words = break_words(sentence)
	print_first_word(words)
	print_last_word(words)

	
# Test the functions
sentence = "Hello world, how are you doing today?"
words = break_words(sentence)
print(words)  # Output: ['Hello', 'world,', 'how', 'are', 'you', 'doing', 'today?']

sorted_words = sort_words(words)
print(sorted_words)  # Output: ['Hello', 'are', 'doing', 'how', 'today?', 'world,', 'you']

print_first_and_last(sentence)  # Output: Hello today?