## Exercise 21: Functions Can Return Something

```
def add(a, b):
	print(f"ADDING {a} + {b}")
	return a + b

age = add(30, 5)
```

- where "age" have two values- a = 30 and b = 5
- it will print as "ADDING 30 + 5"

-- but if you do 
```
def add(a, b):
	return a + b

age = add(30, 5)

print(age)
```
- it will print as "35"

---

## Exercise 25: Even More Practice

Complicated

## Exercise 26 

make sure to make a .txt file and while runing mention name
- ``` python3 ex26.py file_name.txt```

- after runing, will ask for user input 
```
print("How old are you?", end=' ')
age = input()
```

- ``` txt = open(filename)``` this will open the file
-```print(f"Here's your file {filename}:")``` will print file name
- ```print(txt.read())``` will print the text inside the file 

- ```
print("Type the filename again:")
file_again = input("> ")

txt_again = open(file_again)

print(txt_again.read())
```
asking to Type file name again and open it, and reading it 
- ```print('\n newlines and \t tabs.')```
- "\n" is to go next like
- "\t" is for tab function in keyboard

- ```dogs += 5``` this will add +5 in dogs which will be 15 + 5= 20

---

## Exercise for if and Else 

- ```
people = 20
cats = 30
dogs = 15

if people < cats:
    print("Too many cats! The world is doomed!")

if people > cats:
    print("Not many cats! The world is saved!")

if people < dogs:
    print("The world is drooled on!")

if people > dogs:
    print("The world is dry!")
	```
	# or 

- ```
people = 30
cars = 40
trucks = 15

if cars > people:
    print("We should take the cars.")
elif cars < people:
    print("We should not take the cars.")
else:
    print("We can't decide.")
```
---

- for input taking 
```
car = input("> ") #enter the number

if car == "1": #if 1 is pressed
    print("you like GTR")# print this on 1
elif car == "2":# if 2 is pressed
    print("you like Supra") #print this on 2
else: #no 1 or 2, besides that this will work 
    print("you are into JDM") #will print this 
```

---

- this is for the list 

```
the_count = [1, 2, 3, 4, 5]

for number in the_count:
    print(f"THis is count {number}") #this will print 5 times beacuse there are 5 numbers in list 
```

- for adding in list 
```
numbers = []
for i in range(0, 6):
    print(number)
    number.aapend(i)
```
- this code will start from 0 and go up to 6
and it will add 1 by 1

---

Ex35 need to be understand

---

# Exercise 41: Learning to Speak Object-Oriented

1. ``` import random``` is a function to generate random numbers 

2. ``` import sys``` its a [System-specific parameters] (https://docs.python.org/3/library/sys.html) and functions 

3. ``` import urlopen``` fetching URLs (Uniform Resource Locators)
- It can be used to open URLs, read and write data to URLs, and perform other operations related to URLs.
 
4. ``` urllib.request``` [which help in opening URLs] (https://docs.python.org/3/library/urllib.request.html) 
