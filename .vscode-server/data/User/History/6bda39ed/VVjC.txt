## Exercise 21: Functions Can Return Something

```
def add(a, b):
	print(f"ADDING {a} + {b}")
	return a + b

age = add(30, 5)
```

- where "age" have two values- a = 30 and b = 5
- it will print as "ADDING 30 + 5"

-- but if you do 
```
def add(a, b):
	return a + b

age = add(30, 5)

print(age)
```
- it will print as "35"

---

## Exercise 25: Even More Practice

