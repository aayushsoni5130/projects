from random import randint
from sys import exit
from textwrap import dedent

# Define a base class for scenes
class Scene(object):
    def enter(self):
        print("This scene is not yet configured.")
        print("Subclass it and implement enter().")
        exit(1)

# Define a class to manage the game engine
class Engine(object):
    def __init__(self, scene_map):
        self.scene_map = scene_map

    def play(self):
        current_scene = self.scene_map.opening_scene()
        last_scene = self.scene_map.next_scene('finished')

        while current_scene != last_scene:
            next_scene_name = current_scene.enter()
            current_scene = self.scene_map.next_scene(next_scene_name)

        # Display the last scene
        current_scene.enter()

# Define a scene for when the player dies
class Death(Scene):
    quips = [
        "You died. You kinda suck at this.",
        "Your mom would be proud...if she were smarter.",
        "Such a luser.",
        "I have a small puppy that's better at this.",
        "You're worse than your Dad's jokes"
    ]

    def enter(self):
        print(Death.quips[randint(0, len(self.quips) - 1)])
        exit(1)

# Define the Central Corridor scene
class CentralCorridor(Scene):
    def enter(self):
        print(dedent("""
            The Gothons of Planet Percal #25 have invaded your ship and 
            destroyed your entire crew. You are the last surviving 
            member and your last mission is to get the neutron destruct 
            bomb from the Weapons Armory, put it in the bridge, and 
            blow the ship up after getting into an escape pod.
            ...

            You're running down the central corridor to the Weapons 
            Armory when a Gothon jumps out...
            """))
        ...
        # The player's choice logic is implemented here
        
# Define the Laser Weapon Armory scene
class LaserWeaponArmory(Scene):
    def enter(self):
        print(dedent("""
            You do a dive roll into the Weapon Armory...
            ...

            You stand up and run to the far side of
            the room and find the neutron bomb in its container.
            ...
            """))
        ...
        # The player's choice logic for the armory scene
        
# Define the Bridge scene
class TheBridge(Scene):
    def enter(self):
        print(dedent("""
            You burst onto the Bridge with the neutron destruct bomb
            under your arm and surprise 5 Gothons...
            ...

            The player's choice logic for the bridge scene
        ...
        
# Define the Escape Pod scene
class EscapePod(Scene):
    def enter(self):
        print(dedent("""
            You rush through the ship desperately trying to make it to
            the escape pod before the whole ship explodes...
            ...

            The player's choice logic for the escape pod scene
        ...
        
# Define the Finished scene
class Finished(Scene):
    def enter(self):
        print("You won! Good job.")
        return 'finished'

# Define a map that connects different scenes
class Map(object):
    scenes = {
        'central_corridor': CentralCorridor(),
        'laser_weapon_armory': LaserWeaponArmory(),
        'the_bridge': TheBridge(),
        'escape_pod': EscapePod(),
        'death': Death(),
        'finished': Finished()
    }

    def __init__(self, start_scene):
        self.start_scene = start_scene

    def next_scene(self, scene_name):
        val = Map.scenes.get(scene_name)
        return val

    def opening_scene(self):
        return self.next_scene(self.start_scene)

# Initialize the game map and engine
a_map = Map('central_corridor')
a_game = Engine(a_map)

# Start playing the game
a_game.play()
