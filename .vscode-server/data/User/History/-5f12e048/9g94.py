# Import the necessary modules
from random import randint  # Import a function to generate random integers
from sys import exit  # Import the exit function from the sys module
from textwrap import dedent  # Import a function to remove leading whitespace from multi-line strings

# Define a base class for scenes
class Scene(object):
    def enter(self):
        # This method is not yet configured for each specific scene.
        # Print a message indicating that it should be subclassed and implemented.
        print("This scene is not yet configured.")
        print("Subclass it and implement enter().")
        exit(1)  # Exit the program with an error code 1

# Define a class to manage the game engine
class Engine(object):
    def __init__(self, scene_map):
        self.scene_map = scene_map

    def play(self):
        # Start the game by setting the current scene to the opening scene
        current_scene = self.scene_map.opening_scene()
        # Set the last scene to the "finished" scene
        last_scene = self.scene_map.next_scene('finished')

        # Loop until the current scene is the last scene
        while current_scene != last_scene:
            # Get the next scene's name by calling the current scene's enter method
            next_scene_name = current_scene.enter()
            # Get the actual next scene object based on its name
            current_scene = self.scene_map.next_scene(next_scene_name)

        # Display the last scene
        current_scene.enter()

# Define a scene for when the player dies
class Death(Scene):
    # A list of messages for when the player dies
    quips = [
        "You died. You kinda suck at this.",
        "Your mom would be proud...if she were smarter.",
        "Such a luser.",
        "I have a small puppy that's better at this.",
        "You're worse than your Dad's jokes"
    ]

    def enter(self):
        # Display a random death message
        print(Death.quips[randint(0, len(self.quips) - 1)])
        exit(1)  # Exit the program with an error code 1

# Define a class named CentralCorridor that inherits from the Scene class
class CentralCorridor(Scene):
    # Define the enter method for this scene
    def enter(self):
        # Print the story and situation for the Central Corridor scene
        print(dedent("""
			The Gothons of Planet Percal #25 have invaded your ship and 
			destroyed your entire crew. You are the last surviving 
			member and your last mission is to get the neutron destruct 
			bomb from the Weapons Armory, put it in the bridge, and 
			blow the ship up after getting into an escape pod.

			You're running down the central corridor to the Weapons 
			Armory when a Gothon jumps out, red scaly skin, dark grimy 
			teeth, and evil clown costume flowing around his hate 
			filled body. He's blocking the door to the Armory and 
			about to pull a weapon to blast you.
			"""))
        
        # Prompt the player for an action and store their input
        action = input("> ")

        # Check the player's chosen action
        if action == "shoot!":
            # Print the consequences of the "shoot!" action
            print(dedent("""
			    Quick on the draw you yank out your blaster and fire
			    it at the Gothon. His clown costume is flowing and
			    moving around his body, which throws off your aim.
			    Your laser hits his costume but misses him entirely.
			    This completely ruins his brand new costume his mother
			    bought him, which makes him fly into an insane rage
			    and blast you repeatedly in the face until you are
			    dead. Then he eats you.
			    """))
            # Return the scene name 'death' to indicate the player's outcome

        elif action == "dodge!":
            # Print the consequences of the "dodge!" action
            print(dedent("""
				Like a world class boxer you dodge, weave, slip and
				slide right as the Gothon's blaster cranks a laser
				past your head. In the middle of your artful dodge
				your foot slips and you bang your head on the metal
				wall and pass out. You wake up shortly after only to
				die as the Gothon stomps on your head and eats you.
				"""))
            # Return the scene name 'death' to indicate the player's outcome

        elif action == "tell a joke":
            # Print the consequences of the "tell a joke" action
            print(dedent("""
			    Lucky for you they made you learn Gothon insults in
			    the academy. You tell the one Gothon joke you know:
			    Lbhe zbgure vf fb sng, jura fur fvgf nebhaq gur ubhfr,
			    fur fvgf nebhaq gur ubhfr. The Gothon stops, tries
			    not to laugh, then busts out laughing and can't move.
			    While he's laughing you run up and shoot him square in
			    the head putting him down, then jump through the
			    Weapon Armory door.
			    """))
            # Return the scene name 'laser_weapon_armory' to indicate the player's next scene

        else:
            # Print a message for an unrecognized action
            print("DOES NOT COMPUTE!")
            # Return the scene name 'central_corridor' to loop back to this scene
            return 'central_corridor'

# Define a new scene class called LaserWeaponArmory, which is a subclass of Scene
class LaserWeaponArmory(Scene):
    # Define the enter method for this scene
    def enter(self):
        # Display the introduction and description of the scene, using dedent to remove leading whitespace
        print(dedent("""
            You do a dive roll into the Weapon Armory, crouch and scan
            the room for more Gothons that might be hiding. It's dead
            quiet, too quiet. You stand up and run to the far side of
            the room and find the neutron bomb in its container.
            There's a keypad lock on the box and you need the code to
            get the bomb out. If you get the code wrong 10 times then
            the lock closes forever and you can't get the bomb.  The
            code is 3 digits.
            """))

        # Generate a random 3-digit code for the bomb
        code = f"{randint(1,9)}{randint(1,9)}{randint(1,9)}"
        
        # Ask the player to input a guess for the code
        guess = input("[keypad]> ")
        
        # Initialize the number of guesses to 0
        guesses = 0

        # Create a loop that continues until the player guesses the correct code or reaches 10 guesses
        while guess != code and guesses < 10:
            # Display an incorrect guess message
            print("BZZZZEDDD!")
            # Increase the number of guesses
            guesses += 1
            # Ask for another guess
            guess = input("[keypad]> ")

        # Check if the player's guess matches the correct code
        if guess == code:
            # If correct, display a message and return the scene name 'the_bridge'
            print(dedent("""
                The container clicks open and the seal breaks, letting
                gas out. You grab the neutron bomb and run as fast as
                you can to the bridge where you must place it in the
                right spot.
                """))
            return 'the_bridge'
        else:
            # If incorrect, display a message and return the scene name 'death'
            print(dedent("""
                The lock buzzes one last time and then you hear a
                sickening melting sound as the mechanism is fused
                together. You decide to sit there, and finally the
                Gothons blow up the ship from their ship and you die.
                """))
            return 'death'

# Define a new scene class called TheBridge that inherits from the Scene class
class TheBridge(Scene):
    # Define the enter method for the TheBridge scene
    def enter(self):
        # Print the description of the scene using dedent to format the multi-line string
        print(dedent("""
			You burst onto the Bridge with the neutron destruct bomb
			under your arm and surprise 5 Gothons who are trying to
			take control of the ship. Each of them has an even uglier
			clown costume than the last. They haven't pulled their
			weapons out yet, as they see the active bomb under your
			arm and don't want to set it off.
			"""))

        # Prompt the player for an action and store it in the 'action' variable
        action = input("> ")

        # Check the player's input to determine the outcome of their action
        if action == "throw the bomb":
            # Print the result of choosing to throw the bomb
            print(dedent("""
			    In a panic you throw the bomb at the group of Gothons
			    and make a leap for the door. Right as you drop it a
			    Gothon shoots you right in the back killing you.  As
			    you die you see another Gothon frantically try to
			    disarm the bomb. You die knowing they will probably
			    blow up when it goes off.
			    """))
            # Return 'death' to indicate that the player has died
            return 'death'

        elif action == "slowly place the bomb":
            # Print the result of choosing to slowly place the bomb
            print(dedent("""
			    You point your blaster at the bomb under your arm and
			    the Gothons put their hands up and start to sweat.
			    You inch backward to the door, open it, and then
			    carefully place the bomb on the floor, pointing your
			    blaster at it. You then jump back through the door,
			    punch the close button and blast the lock so the
			    Gothons can't get out. Now that the bomb is placed
			    you run to the escape pod to get off this tin can.
			    """))
            # Return 'escape_pod' to indicate that the player proceeds to the escape pod scene
            return 'escape_pod'
        else:
            # Print a message when the player's input doesn't match any of the expected actions
            print("DOES NOT COMPUTE!")
            # Return 'the_bridge' to indicate that the player remains in the same scene
            return 'the_bridge'

# Define a new scene class called EscapePod, inheriting from the Scene class
class EscapePod(Scene):
    # Define the enter method for the EscapePod scene
    def enter(self):
        # Print a multi-line description of the situation and scenario
        print(dedent("""
            You rush through the ship desperately trying to make it to
            the escape pod before the whole ship explodes. It seems
            like hardly any Gothons are on the ship, so your run is
            clear of interference. You get to the chamber with the
            escape pods, and now need to pick one to take.  Some of
            them could be damaged but you don't have time to look.
            There's 5 pods, which one do you take?
            """))

        # Generate a random number between 1 and 5 to determine the good escape pod
        good_pod = randint(1, 5)
        # Prompt the player for input, asking which escape pod they choose
        guess = input("[pod #]> ")

        # Check if the player's chosen pod number is not the same as the good escape pod
        if int(guess) != good_pod:
            # Print a description of the player's unfortunate choice
            print(dedent(f"""
                You jump into pod {guess} and hit the eject button.
                The pod escapes out into the void of space, then
                implodes as the hull ruptures, crushing your body into
                jam jelly.
                """))

            # Return 'death' to indicate that the player has died and the game should continue to the death scene
            return 'death'
        else:
            # Print a description of the player's successful escape
            print(dedent(f"""
                You jump into pod {guess} and hit the eject button.
                The pod easily slides out into space heading to the
                planet below. As it flies to the planet, you look
                back and see your ship implode then explode like a
                bright star, taking out the Gothon ship at the same
                time. You won!
                """))

            # Return 'finished' to indicate that the player has successfully completed the game
            return 'finished'

# Define another scene class called Finished, also inheriting from the Scene class
class Finished(Scene):
    # Define the enter method for the Finished scene
    def enter(self):
        # Print a victory message for the player
        print("You won! Good job.")
        # Return 'finished' to indicate that the game has concluded
        return 'finished'


# Define a class called Map
class Map(object):
    # Define a dictionary of scenes, where scene names are keys and corresponding scene objects are values
    scenes = {
        'central_corridor': CentralCorridor(),
        'laser_weapon_armory': LaserWeaponArmory(),
        'the_bridge': TheBridge(),
        'escape_pod': EscapePod(),
        'death': Death(),
        'finished': Finished()
    }

    # Constructor method for the Map class
    def __init__(self, start_scene):
        # Store the starting scene's name as an instance variable
        self.start_scene = start_scene

    # Method to retrieve the next scene object based on a scene name
    def next_scene(self, scene_name):
        # Retrieve the scene object from the scenes dictionary based on the given scene name
        val = Map.scenes.get(scene_name)
        return val

    # Method to retrieve the opening scene object
    def opening_scene(self):
        # Call the next_scene method with the starting scene name to get the opening scene object
        return self.next_scene(self.start_scene)

# Create an instance of the Map class with 'central_corridor' as the starting scene
a_map = Map('central_corridor')

# Create an instance of the Engine class, passing the Map instance as a parameter
a_game = Engine(a_map)

# Start playing the game using the play method of the Engine instance
a_game.play()

