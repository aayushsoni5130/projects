# Import the necessary modules
from random import randint  # Import a function to generate random integers
from sys import exit  # Import the exit function from the sys module
from textwrap import dedent  # Import a function to remove leading whitespace from multi-line strings

# Define a base class for scenes
class Scene(object):
    def enter(self):
        # This method is not yet configured for each specific scene.
        # Print a message indicating that it should be subclassed and implemented.
        print("This scene is not yet configured.")
        print("Subclass it and implement enter().")
        exit(1)  # Exit the program with an error code 1

# Define a class to manage the game engine
class Engine(object):
    def __init__(self, scene_map):
        self.scene_map = scene_map

    def play(self):
        # Start the game by setting the current scene to the opening scene
        current_scene = self.scene_map.opening_scene()
        # Set the last scene to the "finished" scene
        last_scene = self.scene_map.next_scene('finished')

        # Loop until the current scene is the last scene
        while current_scene != last_scene:
            # Get the next scene's name by calling the current scene's enter method
            next_scene_name = current_scene.enter()
            # Get the actual next scene object based on its name
            current_scene = self.scene_map.next_scene(next_scene_name)

        # Display the last scene
        current_scene.enter()

# Define a scene for when the player dies
class Death(Scene):
    # A list of messages for when the player dies
    quips = [
        "You died. You kinda suck at this.",
        "Your mom would be proud...if she were smarter.",
        "Such a luser.",
        "I have a small puppy that's better at this.",
        "You're worse than your Dad's jokes"
    ]

    def enter(self):
        # Display a random death message
        print(Death.quips[randint(0, len(self.quips) - 1)])
        exit(1)  # Exit the program with an error code 1

# Define the Central Corridor scene
class CentralCorridor(Scene):
    def enter(self):
        # Display the story and situation for the Central Corridor scene
        print(dedent("""
            The Gothons of Planet Percal #25 have invaded your ship and 
            destroyed your entire crew. You are the last surviving 
            member and your last mission is to get the neutron destruct 
            bomb from the Weapons Armory...
            ...
            """))
        # The player's choice logic and consequences are implemented here

# Define the Laser Weapon Armory scene
class LaserWeaponArmory(Scene):
    def enter(self):
        # Display the story and situation for the Laser Weapon Armory scene
        print(dedent("""
            You do a dive roll into the Weapon Armory...
            ...
            """))
        # The player's choice logic and consequences for the armory scene

# ... Similar explanations for the other scene classes

# Define a map that connects different scenes
class Map(object):
    # A dictionary that maps scene names to their corresponding scene objects
    scenes = {
        'central_corridor': CentralCorridor(),
        'laser_weapon_armory': LaserWeaponArmory(),
        'the_bridge': TheBridge(),
        'escape_pod': EscapePod(),
        'death': Death(),
        'finished': Finished()
    }

    def __init__(self, start_scene):
        self.start_scene = start_scene

    def next_scene(self, scene_name):
        # Get the scene object associated with the given scene name
        val = Map.scenes.get(scene_name)
        return val

    def opening_scene(self):
        # Get the opening scene object based on the start scene name
        return self.next_scene(self.start_scene)

# Initialize the game map with the starting scene and create the game engine
a_map = Map('central_corridor')
a_game = Engine(a_map)

# Start playing the game
a_game.play()
