class Song(object):
    def __init__(self, lyrics):
        # The constructor method (__init__) is called when a new instance of the class is created.
        # It takes a parameter 'lyrics', which is a list of strings representing the lines of the song.
        self.lyrics = lyrics

    def sing_me_a_song(self):
        # The 'sing_me_a_song' method is responsible for printing each line of the song from the 'lyrics' list.
        for line in self.lyrics:
            print(line)

# Create instances of the Song class with different sets of lyrics.
happy_bday = Song(["Happy birthday to you",
                   "I don't want to get sued",
                   "So I'll stop right there"])

bulls_on_parade = Song(["They rally around the family",
                        "With pockets full of shells"])

# Call the 'sing_me_a_song' method on each instance to print the song's lyrics.
happy_bday.sing_me_a_song()  # Output the lyrics of the 'Happy Birthday' song.
bulls_on_parade.sing_me_a_song()  # Output the lyrics of the song 'Bulls on Parade'.
