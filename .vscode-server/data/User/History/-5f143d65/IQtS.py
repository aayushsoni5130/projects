class Song(object):
    def __init__(self, lyrics):
        # The constructor method (__init__) is called when a new instance of the class is created.
        # It takes a parameter 'lyrics', which is a list of strings representing the lines of the song.
        self.lyrics = lyrics

    def sing_me_a_song(self):
        # The 'sing_me_a_song' method is responsible for printing each line of the song from the 'lyrics' list.
        for line in self.lyrics:
            print(line)

# Create instances of the Song class with different sets of lyrics.
happy_bday = Song(["""
The stars, the sun, the moon, the sky got nothing on
the way you shine
I'd die for you 'cause every night I know you'll bring
me back to life
I'm floating on a feelin', I'm livin' like I'm dreamin'
If you told me you're an angel out of heaven, baby, I'd
believe
                   """])

bulls_on_parade = Song(["""
The stars, the sun, the moon, the sky got nothing on
the way you shine
I'd die for you 'cause every night I know you'll bring
me back to life
I'm floating on a feelin', I'm livin' like I'm dreamin'
If you told me you're an angel out of heaven, baby, I'd
believe it
                        """])

# Call the 'sing_me_a_song' method on each instance to print the song's lyrics.
happy_bday.sing_me_a_song()  # Output the lyrics of the 'Happy Birthday' song.
bulls_on_parade.sing_me_a_song()  # Output the lyrics of the song 'Bulls on Parade'.
